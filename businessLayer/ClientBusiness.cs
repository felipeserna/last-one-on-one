﻿using dataLayer;
using entityLayer;
using System.Collections.Generic;

namespace businessLayer
{
    public class ClientBusiness
    {
        private ClientData myClient = new ClientData();
        public List<ClientEntity> GetClient()
        {
            return myClient.GetClient();
        }

        public int AddClient(ClientEntity pEntity)
        {
            return myClient.AddClient(pEntity);
        }

        public int UpdateClient(ClientEntity pEntity)
        {
            return myClient.UpdateClient(pEntity);
        }

        public int DeleteClient(int pId)
        {
            return myClient.DeleteClient(pId);
        }
    }
}
