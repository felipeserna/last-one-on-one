﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using entityLayer;

namespace dataLayer
{
    public class ClientData
    {
        private string ConnectionString = ConfigurationManager.ConnectionStrings["CRUD"].ToString();
        public List<ClientEntity> GetClient()
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                cn.Open();
                const string query = @"SELECT Id,Name,LastName,Phone,Email
                                       FROM Client";

                return cn.Query<ClientEntity>(query, CommandType.Text).ToList();
            }
        }

        public int AddClient(ClientEntity pEntity)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                cn.Open();
                const string query = @"INSERT INTO Client (Name,LastName,Phone,Email)
                                       VALUES (@pName,@pLastName,@pPhone,@pEmail)";

                var parameters = new DynamicParameters();
                parameters.Add("@pId", pEntity.Id, DbType.Int32);
                parameters.Add("@pName", pEntity.Name, DbType.String);
                parameters.Add("@pLastName", pEntity.LastName, DbType.String);
                parameters.Add("@pPhone", pEntity.Phone, DbType.String);
                parameters.Add("@pEmail", pEntity.Email, DbType.String);

                return cn.Execute(query, parameters, commandType: CommandType.Text);
            }
        }

        public int UpdateClient(ClientEntity pEntity)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                cn.Open();
                const string query = @"UPDATE Client SET Name = @pName,
                                                         LastName = @pLastName,
                                                         Phone = @pPhone,
                                                         Email = @pEmail

                                                     WHERE Id = @pId";

                var parameters = new DynamicParameters();
                parameters.Add("@pId", pEntity.Id, DbType.Int32);
                parameters.Add("@pName", pEntity.Name, DbType.String);
                parameters.Add("@pLastName", pEntity.LastName, DbType.String);
                parameters.Add("@pPhone", pEntity.Phone, DbType.String);
                parameters.Add("@pEmail", pEntity.Email, DbType.String);

                return cn.Execute(query, parameters, commandType: CommandType.Text);
            }
        }

        public int DeleteClient(int pId)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                cn.Open();

                const string query = @"DELETE FROM Client WHERE Id = @pId";
                var parameters = new DynamicParameters();
                parameters.Add("@pId", pId, DbType.Int32);

                return cn.Execute(query, parameters, commandType: CommandType.Text);
            }
        }
    }
}
