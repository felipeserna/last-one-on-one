﻿using businessLayer;
using entityLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace presentationLayer
{
    public partial class Form1 : Form
    {
        private ClientBusiness myClient = new ClientBusiness();
        private string _action = "new";
        private int _id = 0;
        public Form1()
        {
            InitializeComponent();
        }
        
        private void Form1_Load(object sender, EventArgs e)
        {
            this.GetClient();
        }

        private void GetClient()
        {
            var response = myClient.GetClient();
            this.dgClient.DataSource = response;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            // texts
            this.txtName.Enabled = true;
            this.txtLastName.Enabled = true;
            this.txtPhone.Enabled = true;
            this.txtEmail.Enabled = true;

            // buttons
            this.btnEdit.Enabled = false;
            this.btnDelete.Enabled = false;
            this.btnSave.Enabled = true;
            this.btnNew.Enabled = false;
            this.btnCancel.Enabled = true;

            // clean texts
            this.txtName.Text = "";
            this.txtLastName.Text = "";
            this.txtPhone.Text = "";
            this.txtEmail.Text = "";

            this.txtName.Focus();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (_action == "new")
            {
                ClientEntity client = new ClientEntity()
                {
                    Id = 0,
                    Name = this.txtName.Text,
                    LastName = this.txtLastName.Text,
                    Phone = this.txtPhone.Text,
                    Email = this.txtEmail.Text,
                };

                if (client != null)
                {
                    var response = myClient.AddClient(client);

                    if (response > 0)
                    {
                        MessageBox.Show("Saved ok.", ",Message from systems", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("Save error.", ",Message from systems", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            _action = "edit";

            if (_id > 0)
            {
                this.btnEdit.Enabled = false;
                this.btnDelete.Enabled = false;
                this.btnSave.Enabled = true;
                this.btnNew.Enabled = false;
                this.btnCancel.Enabled = true;

                this.txtName.Enabled = true;
                this.txtLastName.Enabled = true;
                this.txtPhone.Enabled = true;
                this.txtEmail.Enabled = true;
            }
        }
    }
}
